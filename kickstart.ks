# Use text mode install
text

# System language
lang en_US.UTF-8

# Keyboard layouts
keyboard us

# Use network installation
url --url="http://172.131.100.1/inst/BaseOS"

# For Additional packages
repo --name=AppStream --baseurl=http://172.131.100.1/inst/AppStream

# Accept the license
eula --agreed

# System timezone
timezone America/New_York --utc

# Root password
rootpw bluefield

# Disable firewall
firewall --disabled

# Run the Setup Agent on first boot
firstboot --enable

# Do not configure the X Window System
skipx

# Partitioning
zerombr
ignoredisk --only-use=nvme0n1
clearpart --all --initlabel --drives=nvme0n1
part /boot/efi --fstype=efi --size=600
part /boot --fstype=xfs --asprimary --size=1024
part pv.01 --grow
volgroup rhel pv.01
logvol / --vgname=rhel --fstype=xfs --name=root --percent=100 # change to 70% for Microshift

# Bootloader
bootloader --append="console=hvc0 console=ttyAMA0 console=ttyAMA1 earlycon=pl011,0x13010000 ignore_loglevel" --location=mbr --boot-drive=nvme0n1

# Setup tmfifo network (post installation)
network --no-activate --onboot=on --device=eth0 --bootproto=static --ip=192.168.100.2 --netmask=255.255.255.252 --noipv6

# Reboot after installation
reboot

%packages --ignoremissing
@^minimal-environment
@network-server
@standard
@system-tools
vim
git
wget
%end

%post --interpreter /bin/bash --logfile=/root/post-ks.log
# From BF2 installation (TODO: review the need for BF3)
systemctl set-default multi-user.target
systemctl disable firewalld

# Enable consoles
systemctl enable serial-getty@hvc0
systemctl enable serial-getty@ttyAMA0.service
systemctl enable serial-getty@ttyAMA1.service

# BF3 initramfs required modules
echo 'add_drivers+=" sdhci-of-dwcmshc mlxbf_tmfifo virtio_console gpio-mlxbf3 mlxbf-gige pinctrl-mlxbf3 vitesse "' > /etc/dracut.conf.d/bf3.conf
dracut --regenerate-all --force

##
## HACK ALERT!! External kernel install & configuration
##
## Comment out if installing a customized kernel
##
# mkdir /opt/RPMs; cd /opt/RPMs
# wget -r -nd http://172.131.100.1/kernel/
# rpm -ivh --oldpackage *.rpm
# VERSION=$(rpm -q -p "%{VERSION}" kernel-core-*.rpm | sed 's/kernel-core-//')
# dracut --add-drivers \
# 	"mlxbf_tmfifo nvme_common nvme_core sdhci_of_dwcmshc sdhci_pltfm virtio_console" \
# 	--force /boot/initramfs-${VERSION}.img $VERSION
# grubby --set-default=/boot/vmlinuz-$VERSION
%end
